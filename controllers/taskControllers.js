//import the task model in the controllers. So that our controllers or controller functions may have access to our Task Model.
const Task = require("../models/Task");

//module.exports will allow us to add the controllers as methods for our module.
//This controller module can be imported in other files. Modules in JS are considered objects.
module.exports.createTaskController = (req,res) => {

	//Check your request body first:
	console.log(req.body);

	//Task model is a constructor.
	//newTask that will be created from our Task model will have additional methos
	//to be used in our application


	//Mongodb - db.tasks.findOne()
	Task.findOne({name:req.body.name})
	.then(result => {

		if(result !== null && result.name === req.body.name){
			return res.send("Duplicate Task Found")
		} else {

			let newTask = new Task({

				name: req.body.name,
				status: req.body.status
			})

			//.save() is a method from an object created by a model.
			//This will allow us to save the document into the collection.
			//save() can have anonymous function or we can have what we call a then chain:

			//The anonymous function in the save() method is used to handle the error or the proper result/response from mongodb.
			/*
				newTask.save((savedTask,error)=>{
			
					if(error){
						res.send(error)
					} else {
						res.send(savedTask)
					}

				})

			*/

			//.then and .catch chain:
			//.then() is used to handle the proper result/returned value of a function. If the function properly returns a value, we can run a separate function to handle it.
			//.catch() is used to handle/catch the error from the use of a function. So that if there is an error, we can properly handle it separate from the result.

			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));

		}

	})
	.catch(error => res.send(error))

}

module.exports.getAllTasksController = (req,res) => {

	//To be able to query with a collection, we use the Task model and its find() function. This find() function is similar to mongoDB's own find(). This will allow us to connect to our collection and retrieve all documents that matches our criteria.
	//Similar to db.tasks.find({})
	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};

module.exports.getSingleTaskController = (req,res) => {

	console.log(req.params);
	//sample: {id: '61ee3ad190518e3fa695d7ab'}

	//Get a single task document by its id:
	//Model.findById() = db.collection.findOne({_id: "id"})
	//Model.findById(), you only need to pass the id itself to look for the document which matches the id.

	//let id = req.params.id
	//Task.findById(id)

	Task.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));

};

module.exports.updateTaskStatusController = (req,res) => {

	console.log(req.params.id);//check the id captured from the url
	console.log(req.body);//check the request body from the client

	//Model.findByIdAndUpdate() - this will allow us to look for the document we want to update by its id.
	//Model.findByIdAndUpdate() = db.collection.updateOne({_id: "id"},{$set{property: newValue}})

	//Syntax: Model.findByIdAndUpdate(id,{updates},{new:true})

	//The third argument: {new:true}, allows us to return the updated version of the document we were updating. By default, without this argument, findByIdAndUpdate will return the previous state of the document or previous version of the document.

	//updates object will contain the field and the value we want to update.
	let updates = {

		status: req.body.status

	}

	Task.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error));

};